<?php

class views_handler_field_want extends views_handler_field_boolean {

  function init(&$view, &$options) {
    parent::init($view, $options);

    $this->formats = array(
      'only' => array(
        0 => t('Wants'), 
        1 => t('Offers')),
    );
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['type'] = array('default' => 'only');
    unset($options['not']);
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['type']);
    unset($form['not']);
  }
}
